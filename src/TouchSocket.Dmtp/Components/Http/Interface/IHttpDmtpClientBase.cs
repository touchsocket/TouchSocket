﻿using TouchSocket.Http;

namespace TouchSocket.Dmtp
{
    public interface IHttpDmtpClientBase : IHttpClientBase, IDmtpActorObject
    {
    }
}
