﻿using TouchSocket.Sockets;

namespace TouchSocket.Dmtp
{
    public interface ITcpDmtpClientBase : ITcpClientBase, IDmtpActorObject
    {
    }
}
