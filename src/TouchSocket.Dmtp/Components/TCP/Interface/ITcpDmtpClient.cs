﻿using TouchSocket.Sockets;

namespace TouchSocket.Dmtp
{
    public interface ITcpDmtpClient : ITcpDmtpClientBase, ITcpClient
    {
    }
}
